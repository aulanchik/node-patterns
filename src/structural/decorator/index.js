let Customer = require('./Customer');
let {
    InventoryItem,
    GoldenInventoryItem,
    DiamondInventoryItem
} = require('./InventoryItem');

let customer = new Customer('Alex', 4000);

var walkman = new InventoryItem("Walkman", 29.99);
var necklace = new InventoryItem("Necklace", 9.99);

var gold_necklace = new GoldenInventoryItem(necklace);
var diamond_gold_necklace = new DiamondInventoryItem(gold_necklace);

var diamond_walkman = new DiamondInventoryItem(walkman);

customer.purchase(diamond_gold_necklace);
customer.purchase(diamond_walkman);
customer.printStatus();

diamond_walkman.print();
