var fisher = require('./Fisher');

var customer = fisher.clone();
customer.name = 'Hugh';
customer.addListItem('Fishing Rod');

console.log(`${customer.name} : ${customer.shoppingList}`);