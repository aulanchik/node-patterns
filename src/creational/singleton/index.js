var Logger = require('./Logger');
var logger = new Logger().getInstance();

var Customer = require('./Customer');
var inventory = require('./data');
var Store = require('./Store');


var customer = new Customer('Steve', 1200);
var store = new Store('Fishing Paradise', inventory);

console.log(`${logger.count} logs in total.`);
logger.logs.map(log => console.log(`\t ${log.message}`));