class Person {
    constructor(name = 'Mystery Person') {
        this._name = name;
    }

    toString() {
        return JSON.stringify(this);
    }
}

module.exports = Person;