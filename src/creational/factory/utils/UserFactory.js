var Employee = require('../Employee');
var Customer = require('../Customer');

var UserFactory = (name, accountBalance = 0, type, employer) => {
    return type == 'employee' ? new Employee(name, accountBalance, employer) : new Customer(name, accountBalance);
}

module.exports = UserFactory;